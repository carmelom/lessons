\documentclass[10pt, fleqn]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{enumitem}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{siunitx}
\sisetup{list-units = single, list-final-separator = { e }}

\usepackage{geometry}
\geometry{a4paper, top=1.8cm, bottom=2cm, left=2cm, right=2cm}

\usepackage[final]{pdfpages}
\usepackage{multicol}

\newcommand{\newproblem}{~\newline\hrule \subsection{}}
\newcommand{\shortnewproblem}[1]{~\newline \rule[.25\baselineskip]{#1}{.1pt} \subsection{}}
\newcommand{\temp}[1]{$\langle$#1$\rangle$}

\title{Moto vario, moti relativi e forze apparenti\\ \vspace{5pt}\large{Fisica 1 -- a.a. 2015/2016}}
\author{Carmelo Mordini}
\date{}                                           % Activate to display a given date or no date

\begin{document}
	\maketitle

\setcounter{section}{1}
	
\newproblem
Due macchine (chiamiamole 1 e 2) stanno viaggiando su strade ortogonali, e sono dirette rispettivamente verso nord e verso est con velocità rispetto al terreno di \SI{60}{km\per h} e \SI{80}{km \per h}. Calcolare la loro velocità relativa, intesa come la velocità della macchina 2 rispetto alla macchina 1.\\
Si ripeta il problema supponendo che la macchina 2 stia viaggiando verso ovest.

\newproblem
Una barca si muove in direzione N\SI{60}{\degree}W a \SI{4}{km\per h} rispetto all'acqua. La corrente ha una direzione tale che il moto risultante rispetto a terra è verso ovest a \SI{5}{km\per h}. Calcolare la velocità e la direzione della corrente rispetto a terra.

\newproblem
Un fiume scorre verso nord con velocità di \SI{3}{km\per h}. Una barca si muove verso est con una velocità di \SI{4}{km\per h} rispetto all'acqua.
\begin{itemize}
	\item[a)] Calcolare la velocità della barca rispetto a terra.
	\item[b)] Se il fiume è largo \SI{1}{km}, calcolare il tempo necessario per la traversata.
	\item[c)] Qual è la deviazione verso nord della barca quando raggiunge l'altra sponda del fiume?
\end{itemize}

\newproblem
Una pallina si muove lungo una guida parabolica di equazione $y = ax^2/2$. La coordinata $x$ viene percorsa con velocità costante $v_x$, e al tempo $t=0$ la pallina si trova in $x=0$.
\begin{itemize}
	\item[a)] Scrivere la legge oraria per il moto della pallina, vale a dire le funzioni $x(t)$ e $y(t)$.
	\item[b)] Calcolare in funzione della posizione $x$ le componenti cartesiane della velocità e dell'accelerazione.
	\item[c)] Calcolare il modulo delle componenti radiale e tangente dell'accelerazione.
	\item[d)] Calcolare il raggio di curvatura della traiettoria.
\end{itemize}

\newproblem
Data la legge oraria del moto di un corpo $(x(t), y(t))$, calcolate per ciascuno dei casi proposti:\\
-- le componenti cartesiane della velocità e dell'accelerazione;\\
-- il modulo delle componenti radiale e tangente dell'accelerazione;\\
-- il raggio di curvatura della traiettoria (confrontate in particolare i risultati per b) e c)).\\
e, naturalmente, disegnate la traiettoria e i vettori velocità e accelerazione in diversi istanti del moto!
\begin{multicols}{2}
	\begin{itemize}
		\item[a)] 	
		$
		\begin{cases}
		x = vt \\
		y = a \sin(vt / a)
		\end{cases}
		$
		
		\item[c)] 	
		$
		\begin{cases}
		x = R \cos(\omega t) \\
		y = R \sin(\omega t)
		\end{cases}
		$
	\end{itemize}
	
	\columnbreak
	\begin{itemize}
		\item[b)] 	
		$
		\begin{cases}
		x = R \cos(\alpha t^2 /2) \\
		y = R \sin(\alpha t^2 /2)
		\end{cases}
		$
		
		\item[d)] 	
		$
		\begin{cases}
		x = R \cos(\omega t) \\
		y = R \sin(2 \omega t)
		\end{cases}
		$
	\end{itemize}
\end{multicols}

\newproblem
\begin{wrapfigure}[8]{r}{.22\textwidth}
	\vspace{-30pt}
	\centering
	\includegraphics[width=.2\textwidth]{fig/circles.pdf}
\end{wrapfigure}
Due cerchi $C_1$ e $C_2$ sono disposti su un piano, e ciascuno di essi ruota con velocità angolare $\omega$ rispetto ad un sistema inerziale. Il centro di $C_1$ è fisso nel sistema inerziale, mentre il centro di $C_2$ è fisso su $C_1$ come in figura. Una massa $m$ è fissata al cerchio $C_2$; chiamiamo $\vec R(t)$ la posizione di $m$ rispetto al centro di $C_1$. Trovate la forza apparente che si osserva sulla massa in un riferimento con origine sul centro di $C_2$, rotante con frequenza $\omega$.

\shortnewproblem{350pt}
Considerate una massa in rotazione su un \emph{pendolo circolare}: un capo di una corda ideale lunga $l$ è attaccato al soffitto, mentre l'altro è attaccato alla massa $m$, che si muove su una traiettoria circolare e orizzontale. Durante il moto, la corda mantiene un angolo $\theta$ costante rispetto alla verticale. Trovate la velocità angolare della rotazione della massa, considerando le forze agenti sulla massa una volta nel riferimento inerziale in cui ruota, e una volta in un riferimento solidale ad essa. 

\newproblem
\begin{wrapfigure}{l}{.22\textwidth}
	\vspace{-30pt}
	\centering
	\includegraphics[width=.2\textwidth]{fig/bug.pdf}
\end{wrapfigure}
Un hula hoop sta ruotando a velocità $\omega$ costante lungo un diametro. Un piccolo insetto di massa $m$ cammina lungo il cerchio con una velocità angolare $\Omega$ costante come in figura. Sia $\vec F$ la forza totale che il cerchio applica sull'insetto quando esso si trova ad angolo $\theta$ rispetto alla verticale, ed $F_\perp$ la componente di $\vec F$ perpendicolare al piano del cerchio. Trovate $F_\perp$, considerando un sistema di riferimento solidale al cerchio e disegnando tutte le forze (reali e apparenti) percepite dall'insetto in questo riferimento.\\
\emph{[Pro:]} se vi sentite bravi con i vettori, provate a rispondere lavorando nel riferimento inerziale in cui il cerchio ruota, e scrivendo esplicitamente la velocità e l'accelerazione dell'insetto in questo riferimento. Non conviene usare assi cartesiani.

\newproblem
Una pallina scivola sul piano di una giostra, che ruota in senso antiorario con frequenza costante $\omega$. Vista da un riferimento inerziale, la pallina si muove di moto rettilineo uniforme. Mostrate che nel riferimento ruotante solidale al piano della giostra le equazioni del moto per la pallina hanno la forma
\begin{equation*}
	\begin{cases}
	\ddot x = \omega ^2 x + 2\omega \dot y \\
	\ddot y = \omega^2 y - 2\omega \dot x
	\end{cases}
\end{equation*}
e risolvetele date le condizioni iniziali $\vec r (0) = (a, 0)$, $\vec v (0) = (0, v)$.

\newproblem
In una località sulla Terra a latitudine $\theta$, un cannone spara verso est con un angolo di alzo $\alpha$ rispetto al terreno. Calcolate la deviazione verso ovest e verso sud dovute alla forza di Coriolis. In funzione di $\theta$, qual è l'angolo $\alpha_{max}$ che provoca la massima deviazione totale della traiettoria?

\includepdf[pages=1]{2013-05-03_primo_esonero_testo.pdf}
\includepdf[pages=1]{provetta_18_04_2015.pdf}

\end{document}










