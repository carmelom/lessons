\documentclass[10pt, fleqn]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{enumitem}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{siunitx}
\sisetup{list-units = single, list-final-separator = { e }}

\usepackage{geometry}
\geometry{a4paper, top=1.8cm, bottom=2cm, left=2cm, right=2cm}

\title{Esercizi di dinamica I:\\ forze, attriti, corde e carrucole\\ \vspace{5pt}\large{Fisica 1 -- a.a. 2015/2016}}
\author{Carmelo Mordini}
\date{}                                           % Activate to display a given date or no date

\newcommand{\newproblem}{~\newline\hrule \subsection{}}
\newcommand{\shortnewproblem}[1]{~\newline \rule[.25\baselineskip]{#1}{.1pt} \subsection{}}

\begin{document}
	\maketitle

\setcounter{section}{1}

\newproblem
Una massa $m_1 = \SI{3}{kg}$ si trova appoggiata ad un piano inclinato, ed \`e legata ad un'altra massa $m_2 = \SI{2}{kg}$ da una fune ideale (vedi figura). L'inclinazione del piano \`e di \SI{20}{\degree}. 
\begin{figure}[h]
	\centering
	\includegraphics[width=.4\textwidth]{figures/es-1-1.pdf}
\end{figure}
\begin{enumerate}
	\item Disegnate il diagramma delle forze agenti su ciascuna massa, specificandone le componenti lungo gli assi dati in figura.
	\item Calcolate l'accelerazione $a_1$ della massa $m_1$ e la tensione $T$ della fune. Come dipende dai valori delle masse e dell'angolo del piano?
	\item Per quale angolo l'accelerazione cambia segno?
\end{enumerate}

\newproblem
Un volano di diametro \SI{3}{m} inizia a ruotare da fermo, e raggiunge una velocit\`a angolare di \SI{100}{giri\per min} in \SI{4}{s}, con accelerazione angolare uniforme. Una pallina di massa $m = \SI{500}{g}$ \`e incollata sul bordo del disco. 
Calcolare le accelerazioni tangenziale e centripeta della pallina al tempo $t_1 = \SI{2}{s}$. Sapendo che il disco continua ad accelerare indefinitamente con la stessa accelerazione trovata sopra, e che la colla \`e in grado di sostenere una trazione massima di \SI{50}{N}, calcolare in quale istante di tempo la pallina si stacca.


%~\newline \rule[.25\baselineskip]{300pt}{.1pt} \subsection{}
\newproblem
\begin{wrapfigure}[7]{r}{.3\textwidth}
	\vspace{-10pt}
	\includegraphics[width=.3\textwidth]{figures/tavolo.png}
\end{wrapfigure}
A un disco di massa $m$ appoggiato su un tavolo privo di attrito è attaccato un filo che passa attraverso un foro al centro del tavolo e tiene appeso un cilindro di massa $M$. Trovare a che velocità deve girare il disco su un cerchio di raggio $r$ per tener fermo il cilindro.

\newpage
\newproblem
\begin{wrapfigure}{l}{.4\textwidth}
\vspace{5pt}
	\includegraphics[width=.4\textwidth]{figures/cassa.pdf}
\end{wrapfigure}
Un uomo è seduto su una cassa, a cui \`e legata una fune ideale che passa in una carrucola (priva di massa e attrito) e gli ritorna in mano. Se la massa dell'uomo \`e di \SI{70}{kg} e quella della cassa \`e di \SI{120}{kg}, è possibile per lui sollevare la cassa dal pavimento tirando la corda?
\begin{itemize}
	\item Se sì, con quale forza minima deve tirare per sollevare se stesso e la cassa?
	\item Se no, cos'altro succede? In questo caso dunque l'omino si solleva dalla cassa prima che questa si stacchi da terra. Quale forza deve aver applicato per sollevare se stesso?
	\begin{itemize}
	    \item Una volta che si è sollevato e penzola dalla corda, potrà sollevare la cassa? Con quale forza dovrà tirare?
	\end{itemize}
\end{itemize}
Dire come cambiano le precedenti risposte se l'omino, anziché essere semplicemente seduto sulla cassa, si è legato ad essa.

\newproblem
Un artista circense sta per esibirsi nel numero del \emph{giro della morte} in bicicletta. La pista che dovrà percorrere (vedi figura) è circolare, con un raggio $R$ di \SI{2.7}{m}. La massa totale (di lui e della bicicletta) è di \SI{87}{kg}. Per avere un miglior controllo, decide di percorrere il cerchio mantenendo una velocità costante lungo la salita.
\begin{enumerate}
    \item Quanto forte deve pedalare per avere velocità costante? Precisamente, calcolare la forza impressa dalla pedalata in funzione della posizione sulla pista, durante la salita.
    \item Qual è la minima velocità che deve avere ad inizio corsa per rimanere sulla pista per tutto il giro?
\end{enumerate}
Per non stancarsi subito e guadagnare velocità, inizia il numero scendendo senza pedalare da una rampa inclinata \SI{30}{\degree} ed alta \SI{1.5}{m}.
\begin{enumerate}[resume]
    \item La velocit\`a guadagnata lungo la rampa è sufficiente per completare il numero?
\end{enumerate}
\begin{figure}[h]
    \centering
    \includegraphics[width=.5\textwidth]{figures/giromorte.pdf}
\end{figure}


\newproblem
Un oggetto si trova appoggiato sulla cima di una calotta semisferica senza attrito di raggio \SI{30}{cm}. Ad un certo punto, per un lievissimo colpo, inizia a scivolare su un lato: assumiamo che la velocità iniziale sia praticamente 0. A quale angolo si avrà il distacco dalla semisfera? Qual è la velocità al momento del distacco? A che distanza dall'asse della calotta l'oggetto toccherà terra?

\newpage
\newproblem
\begin{wrapfigure}{r}{.4\textwidth}
	\vspace{-30pt}
	\includegraphics[width=.4\textwidth]{figures/blocks2.pdf}
\end{wrapfigure}
Il blocco B della figura pesa \SI{711}{N}, e il coefficiente di attrito statico tra blocco e piano di appoggio è $\mu_s = 0.25$. Trovate il massimo peso del blocco A per cui il sistema è in equilibrio stabile.

~\newline \rule[.25\baselineskip]{240pt}{.1pt} \subsection{}
Due scatole di massa $m_1 = \SI{500}{g}$ e $m_2 = \SI{720}{g}$ sono appoggiate su un piano inclinato. La superficie superiore della prima è orizzontale, e su questa è appoggiata la seconda (vedi figura). Tra la prima scatola e il piano non vi è attrito, mentre ho attrito statico tra le due scatole con un coefficiente $\mu = 0.3$. La prima viene spinta verso l'alto con una forza $F = \SI{0.8}{N}$ parallela al piano inclinato.\\
Supponendo che l'attrito tra le due rimanga sempre statico, qual è l'accelerazione del sistema? Disegnare il diagramma completo delle forze sui due corpi, indicando in particolare l'attrito e tutte le forze vincolari presenti nel sistema.
\begin{figure}[h!]
	\centering
	\includegraphics[width=.4\textwidth]{figures/blocchi.pdf}
\end{figure}


\newproblem
\begin{wrapfigure}{r}{.51\textwidth}
	\flushright
    \includegraphics[width=.5\textwidth]{figures/guida.pdf}
\end{wrapfigure}
Una scatola è appoggiata dentro una guida metallica, con le pareti ad angolo retto. Se la scatola ha massa $m$, disegnate il diagramma di tutte le forze nel sistema e calcolatene i moduli, nel caso in cui
\begin{itemize}[leftmargin=*]
    \item[a)] la guida sia in equilibrio su uno spigolo, con le pareti a \SI{45}{\degree} rispetto al piano orizzontale;
    \item[b)] la guida tocchi a terra con un vertice, abbia lo spigolo sollevato di un angolo $\theta$ rispetto al piano, e le pareti a \SI{45}{\degree} (stavolta rispetto ad un piano parallelo a $y=0$) -- in questo caso tra la scatola e le pareti c'è attrito statico con coefficiente $\mu$: supponete che $\mu$ sia al valore limite per garantire l'equilibrio, e ricavate tale valor limite in funzione degli angoli;
    \item[c)] la guida sia appoggiata sul vertice, con lo spigolo ad angolo $\theta$, e una faccia sia inclinata ad angolo $\varphi$ rispetto al piano $y=0$ -- stessa domanda del punto precedente riguardo al coefficiente d'attrito.
\end{itemize}
Dopo aver risolto il punto c), valutate i casi particolari $\varphi = 0$ e $\varphi = \pi/4$.

\end{document}
