def configure_plt_latex(plt, scale_factor=1., document_fontsize=14, available_width=373.44, columns=1, rows=1, font_offset=-0.5, axis_font_offset=0.0):
    """
    Sets the correct configurations for plots to be included into a latex document
    """
 
    from math import sqrt
    rcParams = plt.rcParams
    
    inches_per_pt = 1.0/72.27
    golden_mean = (sqrt(5)-1.0)/2.0
    fig_width = available_width*inches_per_pt  # width in inches
    fig_height = fig_width*golden_mean 
    
    #rcParams['figure.figsize'] = 7.3*scale_factor, 4.5*scale_factor/columns*rows
    rcParams['figure.figsize'] = 7.3*scale_factor, 4.5*scale_factor/columns*rows
    #rcParams['figure.figsize'] = fig_width*scale_factor, fig_height*scale_factor/columns*rows

#    rcParams['legend.fancybox'] = True
#    rcParams['legend.frameon'] = True
#    rcParams['legend.shadow'] = True
    
#    rcParams['lines.markersize'] = 5
    rcParams['font.size'] = document_fontsize+font_offset
    rcParams['axes.titlesize'] = document_fontsize+font_offset+axis_font_offset
    rcParams['axes.labelsize'] = document_fontsize+font_offset+axis_font_offset
    rcParams['xtick.labelsize'] = document_fontsize+font_offset-1
    rcParams['ytick.labelsize'] = document_fontsize+font_offset-1
    rcParams['legend.fontsize'] = document_fontsize+font_offset-1
    
    rcParams['font.family'] = 'sans-serif'
#    rcParams['font.sans-serif'] = []
#     rcParams['font.sans-serif'] = ["DejaVu Sans"]
    
#     rcParams['font.family'] = 'serif'
#     rcParams['font.serif'] = []
#     rcParams['font.serif'] = ['cm10']
    
    rcParams['text.usetex'] = True
    rcParams['text.latex.unicode']=True
    rcParams['text.latex.preamble'] = r"""
    \usepackage[T1]{fontenc}
    \usepackage{siunitx}
    \usepackage{amsmath}
    \usepackage{amsfonts}
    %\usepackage{amssymb}
    %\usepackage{braket}
    %\usepackage{cmbright}

    %\usepackage{tgheros}
    %\usepackage[tx]{sfmath}
    \sisetup{math-rm = \mathtt,}"""# math-micro = \text{\textmu},}

def reprint_labels(ax1, typex=float, typey=float, typexy=None, size=14):
    if typexy is not None:
        typex = typexy
        typey = typexy
    fontProperties = {'family':'sans-serif',#'sans-serif':['Helvetica'],
    'weight' : 'normal', 'size' : size}
    
    ax1.set_xticklabels(ax1.get_xticks().astype(typex), fontProperties)
    ax1.set_yticklabels(ax1.get_yticks().astype(typey), fontProperties)
    

def savefig(filename, pad=0.6):
    """
    Finalize and save the images
    """
    
    plt.tight_layout(pad=pad)
    plt.savefig(filename+".svg", bbox_inches='tight')
    plt.savefig(filename+".pdf", bbox_inches='tight')
    plt.show()

