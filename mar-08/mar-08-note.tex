\documentclass[10pt, fleqn]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsthm}

\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{nccmath}

\usepackage{enumitem}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{siunitx}
\sisetup{list-units = single, list-final-separator = { e }}

\usepackage{geometry}
\geometry{a4paper, top=1.8cm, bottom=2cm, left=2cm, right=2cm}


\title{Note dalla lezione\\ \large{Fisica 1 -- a.a. 2015/2016}}
\author{Carmelo Mordini}
\date{8 marzo 2016}                                           % Activate to display a given date or no date

\newcommand{\newproblem}{~\newline\hrule \subsection{}}
\newcommand{\temp}[1]{$\langle$#1$\rangle$}
\newcommand{\vv}{\vec V}

\begin{document}
	\maketitle

\section*{Il moto circolare}
Se un corpo si muove lungo una circonferenza, vuol dire che la sua distanza dal centro $O$ (su cui fissiamo l'origine delle coordinate) rimane costante ed uguale al raggio dell'orbita $R$. Analizzeremo il moto da diversi punti di vista, che avete già intravisto nelle spiegazioni a lezione e/o nel corso degli esercizi, che ora vedremo di far conciliare tra loro. Le conclusioni che otterremo, che vi anticipo qua sopra, sono:
\begin{enumerate}
	\item il vettore \emph{posizione} $\vec r$ ha modulo costante e uguale al raggio $R$;
	\item la \emph{velocità} $\vec v$ è sempre tangente alla circonferenza;
	\item l'accelerazione $\vec a$
		\begin{itemize}
		    \item ha sempre una componente \emph{centripeta}, diretta radialmente, responsabile della variazione della direzione della velocità. Nel caso di moto circolare uniforme, il modulo della velocità è costante e l'unica accelerazione presente è la centripeta.
		    \item nel caso in cui il modulo della velocità possa variare, l'accelerazione ha anche una componente \emph{tangenziale} concorde alla velocità.
		\end{itemize}
\end{enumerate}
\subsection*{Coordinate cartesiane}
\begin{wrapfigure}{l}{.4\textwidth}
    \includegraphics[width=.4\textwidth]{plot/motocirc1.pdf}
	\caption{}
    \label{fig:1}
\end{wrapfigure}
Il moto del corpo avviene lungo una curva unidimensionale (la circonferenza): è sufficiente dunque un singolo parametro per indicare la posizione, istante per istante. Nel riferimento dato in figura \ref{fig:1}, le componenti di $\vec r$ sono
\begin{align*}
        &x(t) = R\,\cos\theta(t) \\
		&y(t) = R\,\sin\theta(t)
\end{align*}
e tutta la dipendenza dal tempo sta nell'angolo $\theta$. Il modulo vale 
\begin{equation*}
	r = \sqrt{x^2 + y^2} = R
\end{equation*}
indipendente da $\theta$, ed è diretto secondo il \emph{versore} $(\cos\theta,\ \sin\theta)$.
Calcolo le componenti della velocità derivando rispetto a $t$\footnote{Una notazione molto usata (e molto pratica) è di annotare con un puntino la derivata rispetto al tempo: $\dot x \equiv \tfrac{dx}{dt}$. Comincerò ad utilizzarla più o meno ovunque, per cui ricordiamocelo.}:
\begin{align*}
        &v_x = \dot x = -R\dot \theta\sin\theta \\
		&v_y = \dot y = R\dot \theta \cos\theta
\end{align*}
Il modulo vale 
\begin{equation*}
v = \sqrt{(R\dot\theta\sin\theta)^2 + (R\dot\theta\cos\theta)^2} = R\dot\theta
\end{equation*}
è diretto secondo il versore $(- \sin\theta,\ \cos\theta)$, ortogonale al versore di $\vec r$ (disegnateli, poi provate a calcolare il prodotto scalare tra i due).\\
Per calcolare l'accelerazione deriviamo le componenti della velocit\`a rispetto a $t$. Vi faccio notare che, in ciascuna delle due componenti, vi sono due fattori che dipendono esplicitamente dal tempo: $\sin\theta$ (o $\cos\theta$) e $\dot \theta$, che nel caso generale potrebbe non essere costante; si calcola quindi la derivata del prodotto
\begin{equation*}
\begin{aligned}
		&a_x = -R \dot \theta(\tfrac{d}{dt}\sin\theta) - R  (\tfrac{d}{dt}\dot \theta)\sin \theta = &-R{\dot \theta}^2\cos\theta \ - R \ddot \theta\sin\theta\\
		&a_y = R \dot \theta(\tfrac{d}{dt}\cos\theta) + R  (\tfrac{d}{dt}\dot \theta)\cos \theta = &-R{\dot \theta}^2\sin\theta \ + R \ddot \theta\cos\theta
\end{aligned}
\end{equation*}
Riscriviamo il vettore completo, con i versori $\hat i$ e $\hat j$ degli assi cartesiani, e riorganizziamo i termini:
\begin{align*}
	\vec a &= \left(-R{\dot \theta}^2\cos\theta \ - R \ddot \theta\sin\theta\right)\hat i + \left(- R{\dot \theta}^2\sin\theta \ + R \ddot \theta\cos\theta\right)\hat j \\
		&= \underbrace{R{\dot \theta}^2\left(-\cos\theta \ \hat i - \sin\theta\ \hat j \right)}_{\vec a_c\ \text{centripeta}}
		 + \underbrace{R\ddot \theta \left(-\sin\theta\  \hat i + \cos\theta \ \hat j \right)}_{\vec a_t\ \text{tangenziale}}
\end{align*}
Ecco che $\vec a$ si rivela composto da due termini. Il primo ha modulo $R{\dot{\theta}}^2$, ed è diretto verso $(-\cos\theta,\ -\sin\theta)$ -- opposto al vettore posizione $\vec r$: è l'accelerazione centripeta. Il secondo ha modulo $R\ddot{\theta}$, ed è diretto verso $(-\sin\theta,\ \cos\theta)$, parallelo ala velocità e dunque tangente alla circonferenza: lo chiamiamo accelerazione tangenziale (appunto).\\
In figura \ref{fig:2} sono mostrati i vettori $\vec{r}$, $\vec{v}$, $\vec{a_c}$ e $\vec{a_t}$ con le loro componenti.
\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{plot/motocirc2.pdf}
	\caption{}
	\label{fig:2}
\end{figure}

Come tornare nel caso particolare del moto circolare uniforme?\\
La variazione dell'angolo $\theta$ è la quantità fisica che abbiamo definito \emph{velocità angolare}; analogamente, l'\emph{accelerazione angolare} sarà definita come la derivata della velocità angolare.
\begin{ceqn}
	\begin{equation*}
	\omega(t) \equiv \frac{d\theta}{dt} = \dot \theta \qquad\qquad \alpha(t) \equiv \frac{d\omega}{dt} = \ddot \theta
	\end{equation*}
\end{ceqn}
Il moto circolare è uniforme se la velocità angolare è costante, ovvero se l'accelerazione angolare è nulla. La conseguenza di ciò è che la velocità $\vec v$ varia solo in direzione, e non in modulo; quindi, l'accelerazione (la sua derivata) può essere solo perpendicolare alla velocità, dunque parallela ad $\vec r$.\\
Ponendo $\dot\theta (t) = \omega$ costante, troviamo che l'accelerazione tangenziale si annulla (perché proporzionale a $\ddot \theta = 0$), e ritroviamo le relazioni note tra i moduli delle grandezze cinematiche:
\begin{equation*}
	\begin{cases}
		v = \omega R\\
		a = |\vec a_c| = \omega^2 R = \dfrac{v^2}{R}\\
		\vec a_t = 0
	\end{cases}
\end{equation*}

\subsection*{Vettori}
	Dopo questa barca di calcoli e derivate, cerchiamo di riassumere un po' la situazione. \`E chiaro che la caratteristica fondamentale del moto circolare è\dots di essere circolare! Ovvero, indipendentemente dalla dipendenza temporale di $\theta$, il modulo del vettore posizione non può mai cambiare. E, in generale, se un vettore varia senza cambiare mai il suo modulo, la sua derivata deve sempre restare perpendicolare ad esso\footnote{Già visto svariate volte qua sopra. Se non ne avete abbastanza, in fondo a queste pagine vi propongo anche una dimostrazione rigorosa del fatto.}.
\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{plot/vec3d.pdf}
	\caption{}
	\label{fig:3}
\end{figure}
Possiamo riscrivere le relazioni viste sopra in termini di vettori, e slegarle dal sistema di riferimento cartesiano in cui le abbiamo ricavate. Per fare questo, definiamo un vettore $\vec \omega$ associato alla velocità angolare in questo modo:
\begin{itemize}
	\item la direzione è \underline{fissata} e perpendicolare al piano della circonferenza;
	\item il modulo è uguale alla velocità angolare $\dfrac{d\theta}{dt}$;
	\item il verso è associato al verso di percorrenza della circonferenza in base alla regola della mano destra: fissata la circonferenza sul piano $xy$, orientiamo $\vec \omega$ nel verso positivo dell'asse $z$ se il verso è antiorario ($\theta$ aumenta col tempo -- $\dot{\theta}$ è positivo), e nel verso negativo se la rotazione è oraria ($\dot{\theta} < 0$).
\end{itemize}
Da ora potremo chiamare ``velocità angolare'' direttamente il vettore $\vec\omega$, e $\omega(t)$ il suo modulo.
La relazione che lega $\vec{\omega}$, $\vec{r}$ e $\vec{v}$ è
\begin{ceqn}
	\begin{equation*}
		\vec v = \vec \omega \times \vec r
	\end{equation*}
\end{ceqn}
Per le regole del prodotto vettoriale, la direzione di $\vec v$ è ortogonale ad entrambi i fattori del prodotto, e in particolare è ortogonale a $\vec r$; dato che abbiamo costruito $\vec \omega$ esplicitamente perpendicolare a $\vec r$, il modulo di $\vec v$ è semplicemente il prodotto dei moduli (per il seno dell'angolo compreso, che fa 1)
%\begin{ceqn}
	\begin{equation*}
	|\vec v| = |\vec \omega| |\vec r| = \omega(t)\, R
	\end{equation*}
%\end{ceqn}
stessa relazione che abbiamo trovato sopra. In figura \ref{fig:3} sono mostrati tutti i vettori con il loro orientamento reciproco.
\newline ~\newline
Per il moto circolare non uniforme ammettiamo che il modulo di $\vec \omega$ possa variare, ma non la sua direzione. Questo vuol dire che la derivata di $\vec \omega$ (anch'esso un vettore) deve essere necessariamente parallelo a lui. Introduciamo dunque il vettore ``accelerazione angolare''
\begin{ceqn}
	\begin{equation*}
	\vec\alpha(t) \equiv \frac{d\vec\omega}{dt}
	\end{equation*}
\end{ceqn}
parallelo ad $\vec \omega$, e il cui modulo vale $\alpha(t) = \tfrac{d\omega}{dt} = \ddot \theta$\\
Calcoliamo ora la derivata di $\vec v$ per trovare l'accelerazione:
\begin{align*}
	\vec a  = \dfrac{d\vec v}{dt} &= \dot{\vec \omega} \times \vec r + \vec \omega \times \dot{\vec r} = \vec \alpha \times \vec r + \vec \omega \times \vec v
\end{align*}
da cui riconoscete le due componenti dell'accelerazione:
\begin{itemize}
	\item[--] il primo termine $\vec \alpha \times \vec r$ è perpendicolare ad $\vec r$, concorde a $\vec v$, ed ha modulo $\alpha r = R\ddot \theta$: è l'accelerazione tangenziale;
	\item[--] il secondo termine $\vec \omega \times \vec v$ è perpendicolare ad $\vec v$, parallelo ad $\vec r$ e diretto verso il centro, ed ha modulo $\omega v = \omega \cdot \omega R = \omega^2\,R$: è l'accelerazione centripeta.
\end{itemize}
\begin{ceqn}
	\begin{align*}
	&\vec a_t = \vec \alpha \times \vec r\\
	&\vec a_c = \vec \omega \times \vec v
	\end{align*}
\end{ceqn}

Nel moto circolare uniforme, $|\vec \omega|$ rimane costante, $\vec \alpha = 0$ e $\vec a_t$ di conseguenza si annulla.
%\subsection*{[Bonus] Coordinate polari}

\subsection*{Un po' di geometria}
Come potete immaginare, se avete un vettore che dipende dal tempo, potete calcolare la derivata di ciascuna delle componenti e quello che otterrete sarà sempre un vettore. Come questi due vettori, chiamiamoli $\vec V$ e $\dot{\vec V}$, siano posizionati l'uno rispetto all'altro, ci dà delle informazioni su cosa del vettore $\vec V$ stia cambiando, istante per istante.\\
I fatti che sono stati stressati nelle note precedenti, e che ci hanno aiutato a capire la cinematica del moto circolare, sono:
\begin{enumerate}
	\item[\textbf{1.}] Se di un vettore varia solo la direzione, ma non il modulo, la sua derivata è perpendicolare ad esso;
	\item[\textbf{2.}] Se di un vettore varia solo il modulo, ma non la direzione, la sua derivata è parallela ad esso.
\end{enumerate}
Segue dimostrazione, per i più audaci.
\begin{proof}[\textbf{1.}]
	Calcoliamo il modulo quadro del vettore, facendone il prodotto scalare con se stesso: $V^2 = \vv \cdot \vv$. Se il modulo di $\vv$ è costante, il modulo quadro lo sarà a maggior ragione. Dunque
	\begin{equation*}
		\dfrac{d}{dt} V^2 = 0
	\end{equation*}
	Sviluppando la derivata del prodotto,
	\begin{equation*}
		\dfrac{d}{dt} (\vv\cdot\vv) = \dot \vv \cdot \vv + \vv \cdot \dot\vv = 2 \dot \vv \cdot \vv = 0
	\end{equation*}
	Da cui concludiamo $\dot \vv \cdot \vv = 0$. Ma se per due vettori il prodotto scalare \`e zero, vuol dire che sono ortogonali.
\end{proof}

%\begin{proof}[\textbf{2.}]
%	Bisogna dire che il fatto in questione \`e abbastanza intuitivo. Ci\`o nonostante, si pu\`o formalizzare.\\
%	In ogni istante la direzione di $\vv$ pu\`o essere indicata dal \emph{versore} $\hat u = {\vv}/{|\vv|}$, un vettorino di modulo unitario parallelo a $\vv$.	Ho trovato quindi un modo per riscrivere il vettore $\vv$ separando modulo e direzione:
%	\begin{equation*}
%		\vv = V\hat u
%	\end{equation*}
%	dove in generale entrambi i fattori possono variare nel tempo. Da questa espressione provo a calcolare la derivata
%	\begin{equation*}
%		\dot \vv = \dot V\, \hat u + V\, \dfrac{d}{dt}\hat u
%	\end{equation*}
%	Fin qui \`e tutto molto generale.\\
%	Il punto importante \`e che la derivata del versore $\hat u$ deve essere perpendicolare ad esso: infatti il versore ha modulo costante $\hat u \cdot \hat u = 1$, per cui possiamo ripetere la dimostrazione del punto 1. Ritroviamo anche il fatto che, se il modulo di $\vv$ non varia, il primo addendo l\`i sopra \`e zero e $\dot \vv$ diventa perpendicolare a $\vv$.\\
%	Se invece 
%	
%	
%\end{proof}

\end{document}
