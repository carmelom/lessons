\documentclass[10pt, fleqn]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{enumitem}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{siunitx}
\sisetup{list-units = single, list-final-separator = { e }}

\usepackage{geometry}
\geometry{a4paper, top=1.8cm, bottom=2cm, left=2cm, right=2cm}

\title{Esercizi di cinematica -- I\\ \large{Fisica 1 -- a.a. 2015/2016}}
\author{Carmelo Mordini}
\date{}                                           % Activate to display a given date or no date

\newcommand{\newproblem}{~\newline\hrule \subsection{}}

\begin{document}
	\maketitle

\setcounter{section}{1}

\newproblem
\begin{wrapfigure}{r}{.4\textwidth}
	\flushright
	\vspace{-40pt}
	\includegraphics[width=.4\textwidth]{plot/es1.pdf}
\end{wrapfigure}
Un corpo puntiforme si muove lungo una retta, partendo  al tempo $t=0$ dall'origine $x=0$. La velocit\`a del corpo in funzione del tempo \`e riportata nel seguente grafico.
Calcolare la posizione del corpo negli istanti $t_1 = \SI{1.5}{s}$, $t_2 = \SI{3}{s}$, $t_3 = \SI{4.5}{s}$, $t_4 = \SI{6}{s}$, e disegnare il grafico dell'accelerazione $a(t)$ e della legge oraria $x(t)$.
\vspace{20pt}

\newproblem
Un corpo puntiforme si muove su una retta, partendo al tempo $t=0$ dall'origine $x=0$ (come sopra). Data l'espressione della legge oraria $x(t)$
\begin{equation*}
	x(t) = \displaystyle \begin{cases}
				vt &\quad 0<t\leq t_1\\
				v(t-t_1) - \frac{1}{2}a(t-t_1)^2 + x_0 & \quad t_1 < t \leq t_2\\
				x_0 e^{- \lambda(t-t_2)} &\quad t>t_2
	       \end{cases}
\end{equation*}
calcolare la velocit\`a, l'accelerazione e disegnarne i grafici. Dati: $t_1 = \SI{2}{s}$, $t_2 = \SI{5}{s}$, $x_0 = \SI{6}{m}$, $v = \SI{3}{m\per s}$, $\lambda = \SI{0.5}{s^{-1}}$ [\emph{Hint:} calcolate $x$, $v$ e $a$ nei momenti ``importanti'' del moto ($t_1$ e $t_2$)].

\newproblem
Una pallina viene spinta dal bordo di un tavolo di altezza $H = \SI{1}{m}$, con velocità iniziale $v_0 = \SI{0.5}{m\per s}$ diretta in orizzontale.
\begin{enumerate}
    \item La pallina cade sotto l'accelerazione di gravit\`a $g = \SI{-9.81}{m\per s^2}$ e arriva a terra. Quale traiettoria percorre? Dopo quanto tempo tocca terra? Chiamiamo $x_0$ la distanza orizzontale percorsa prima di toccare terra. Quanto vale?
\end{enumerate}
Dopo aver toccato terra, la pallina rimbalza. Non vogliamo entrare nei dettagli dell'urto col pavimento, ci basta specificare le due componenti della velocità della pallina immediatamente dopo l'urto:\\ \\
CASO A: la componente orizzontale rimane invariata; la componente verticale cambia verso e rimane invariata in modulo.
\begin{enumerate}[resume]
    \item Descrivere il moto dopo l'urto. A che altezza risale la pallina? Quanto spazio percorre in orizzontale prima di toccare terra un'altra volta?
\end{enumerate}
CASO B: come prima, la componente orizzontale rimane invariata; la componente verticale cambia verso, ma poich\'e il pavimento non è perfettamente elastico, il suo modulo si riduce di un fattore $\eta = 0.8$.
\begin{enumerate}[resume]
	\item Stesse domande di prima: calcolare $H_1$, l'altezza raggiunta dopo l'urto, in funzione di $H$ e $\eta$. Calcolare $x_1$, lo spazio percorso prima del secondo rimbalzo, in funzione di $x_0$ e $\eta$.
	\item Dopo infiniti rimbalzi, si fermerà la pallina? E quanto spazio avrà percorso?
\end{enumerate}

\newproblem
Un atleta è in gara su una pista rettilinea. Calcolate la velocità media per questi due casi:
\begin{itemize}
    \item[a)] marcia per \SI{80}{m} a \SI{1.2}{m\per s} e poi corre per altri \SI{80}{m} a \SI{3}{m\per s};
    \item[b)] marcia per \SI{1}{min} a \SI{1.2}{m\per s} e poi corre ancora per \SI{1}{min} a \SI{3}{m\per s}.
\end{itemize}
Disegnate inoltre la curva $x(t)$ per i due casi e indicare come si trova graficamente la velocità media.

\newproblem
La posizione di un oggetto che si muove in linea retta è data dall'espressione $x = 3t + 4t^2 - 3t^3$, dove $x$ è in metri e $t$ in secondi.
\begin{enumerate}
	\item Qual è la sua posizione per $t =$ \SIlist{1;2;3;4}{s}?
	\item Qual è lo spostamento dell'oggetto nell'intervallo tra $t=0$ e $t=\SI{4}{s}$?
	\item Qual è la velocità media nell'intervallo tra $t=\SI{2}{s}$ e $t=\SI{4}{s}$?
	\item Tracciate la curva $x(t)$ per $0\leq t \leq \SI{4}{s}$ e costruite sul grafico la risposta al quesito 3.
\end{enumerate}%\vspace{-\baselineskip}

\newproblem
La posizione di una particella che si muove lungo l'asse $x$ è data in \si{cm} dalla relazione $x = 9.75 + 1.50 t^3$, dove $t$ è in secondi. Considerando l'intervallo tra $t = \SI{2.00}{s}$ e $t = \SI{3.00}{s}$, calcolate
\begin{enumerate}
    \item la velocità media;
    \item la velocità istantanea per $t =$ \SIlist{2.00;2.50;3.00}{s};
    \item il tempo al quale la particella si trova a metà strada tra le sue posizioni per $t = \SI{2.00}{s}$ e $t = \SI{3.00}{s}$;
    \item riportate su un grafico le risposte disegnando la curva $x(t)$.
\end{enumerate}%\vspace{-\baselineskip}

\newproblem
Due treni, che viaggiano alla stessa velocità di \SI{30}{km\per h}, sono diretti l'uno contro l'altro sullo stesso binario rettilineo. Quando si trovano alla distanza di \SI{60}{km} una mosca parte dalla testa del primo treno, e si dirige verso l'altro alla velocit\`a di \SI{60}{km\per h}. Quando lo ha raggiunto, si volta e torna verso il primo treno, e così di seguito. Quanti viaggi può fare la mosca avanti e indietro prima che i due treni si scontrino (e lei rimanga schiacciata)? Qual è la distanza totale da lei percorsa?

\newproblem
Un osservatore lascia cadere un sasso in un pozzo all'istante $t=0$. Dopo un tempo $t_1 = \SI{5.1}{s}$ sente il rumore del sasso che cade sul fondo. Sapendo che la velocità del suono è di \SI{341}{m\per s}, calcolate la profondità del pozzo.

\newproblem
Un punto materiale si muove nel piano su una guida descritta dall'equazione
\begin{equation*}
	y = A \sin(kx)
\end{equation*}
mantenendo costante la propria velocità lungo $x$, $v_x = v_0$.\\
Calcolare il valore massimo e minimo del modulo della velocità, e il valore massimo e minimo del modulo dell'accelerazione. Riportare sulla traiettoria i punti corrispondenti a questi valori.

\newproblem
Un proiettile viene sparato con velocità di \SI{600}{m\per s} e con un angolo di tiro di \SI{60}{\degree} rispetto all'orizzonte. Calcolate
\begin{enumerate}
    \item la gittata;
    \item la quota massima;
    \item la velocità e la quota dopo \SI{30}{s};
    \item la velocità del proiettile quando si trova a \SI{10}{km} di altezza e il tempo impiegato per raggiungere tale quota.
\end{enumerate}

\newproblem
Durante un safari, un cacciatore avvista una scimmia e tenta di colpirla. La scimmia si trova in cima a un albero, a una distanza di \SI{120}{m} e a un'altezza di \SI{9}{m}. Il cacciatore, (forse) inesperto, punta il fucile direttamente in direzione della scimmia e spara; nell'istante dello sparo, udito il rumore\footnote{trascuriamo il tempo di propagazione del suono, e il tempo di reazione della scimmia.}, la scimmia si lascia cadere dall'albero. Sapendo che il proiettile viene sparato a una velocità di \SI{90}{m\per s}, la scimmia verrà colpita? Se sì, dopo quanto tempo?

\newproblem
Una nave pirata è ormeggiata a \SI{560}{m} da un forte che difende l'entrata del porto di un'isola. Il cannone che la protegge, piazzata al livello del mare, ha una velocità di uscita dei proiettili (alla volata) di \SI{82}{m\per s}. A quale \emph{alzo} (angolo di elevazione) si deve puntare il cannone per colpire la nave pirata? Per ciascun angolo trovato, quale sarà il tempo di volo della palla di cannone?\\
Prima che si cominci a sparare, la nave comincia a muoversi parallelamente alla riva alla velocità di 5 nodi\footnote{il \emph{nodo} è un'unità di misura per la velocità comunemente usata in ambito nautico, e corrisponde a \SI{1.852}{km\per h}}. Dopo quanto tempo la nave sarà fuori dalla portata del cannone?

\newproblem
Un coniglio si muove arbitrariamente nel piano mantenendo il modulo della sua velocità $v_c$ costante. Una volpe lo insegue muovendosi anche essa con velocità costante in modulo $v_v$, dirigendosi istante per istante nella direzione del coniglio. Dimostrare che indipendentemente dalla traiettoria scelta dal coniglio esso verrà raggiunto in un tempo finito se $v_v > v_c$.



\end{document}
